#!/bin/sh
mkdir ./pkgs/
echo 'LYNX: Se crean pkgs'
mkdir ./pkgs/debs/
echo 'LYNX: Se crean debs'
# Configuracion de reprepro
mkdir ./pkgs/debs/conf
touch ./pkgs/debs/conf/{option,distributions}
echo 'Codename: lynxos' >> pkgs/debs/conf/distributions
echo 'Components: main' >> pkgs/debs/conf/distributions
echo 'Architectures: amd64 i386 arm64 armhf' >> pkgs/debs/conf/distributions
echo 'SignWith: F2D8823851EC4786' >> pkgs/debs/conf/distributions
echo -e 'LYNX: Se Configura distributions'


echo -e "\e[0;32mSign the repositories"
# extract the public and private GPG keys from encrypted archive keys.tar with
# the secret openssl pass KEY, which is stored in GitlabCI variables
#openssl aes-256-cbc -d -in keys.tar.enc -out keys.tar -k $KEY
# unzip -P $PASS keys.zip
#signing the repository metadata with my personal GPG key
gpg2 --import keys/LynxPub.gpg && gpg2 --import keys/LynxPriv.gpg
expect -c "spawn gpg2 --edit-key 7216986FBB6380E3CCE05685F2D8823851EC4786 trust quit; send \"5\ry\r\"; expect eof"
echo 'LYNX: Se Importa Clave'

# generation of new deb repository
echo 'LYNX: Se Configura Repo INICIO'
reprepro -V -b pkgs/debs includedeb lynxos debinstall/*deb
echo 'LYNX: Se Configura Repo FIN'

ls pkgs/debs/dists/lynxos/main


echo -e "\e[0;32mList of imported public and private keys:"
gpg2 --list-keys && gpg2 --list-secret-keys

echo 'LYNX: Se Clona Repo'
git clone https://oauth2:P1QgfXfPBU5XARxuy8sW@gitlab.com/LynxOS/repository.git newrepo
cd newrepo && git checkout -b lynxos

git filter-branch --tree-filter 'find . -name "*.deb" -exec rm {} \;'

echo 'LYNX: Se acomoda repo'
rm -rf debs && mv ../pkgs/* .


git add .
git -c user.name='GitlabCI' -c user.email='gitlab@gitlab.com' commit -m '[ci skip] Update repository'

git push -f origin lynxos

#git push -f https://oauth2:P1QgfXfPBU5XARxuy8sW@gitlab.com/LynxOS/repository.git lynxos
